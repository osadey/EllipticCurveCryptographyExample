package com.oodrive.elliptic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptData {
	
	private Cipher cipher;
	
    // AES-GCM parameters
    public static final int GCM_TAG_LENGTH = 16; // in bytes
    
	public EncryptData(File originalFile, File encrypted, SecretKeySpec secretKey, byte[] nonce) throws IOException, GeneralSecurityException{
        
		this.cipher = Cipher.getInstance("AES/GCM/NoPadding", "SunJCE");
		encryptFile(getFileInBytes(originalFile), encrypted, secretKey, nonce);
	}
	
	public void encryptFile(byte[] input, File output, SecretKeySpec key, byte[] nonce) throws IOException, GeneralSecurityException {
        
        GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce);
        this.cipher.init(Cipher.ENCRYPT_MODE, key, spec);
        
		writeToFile(output, this.cipher.doFinal(input));
    }
	
	private void writeToFile(File output, byte[] toWrite) throws IllegalBlockSizeException, BadPaddingException, IOException{
		output.getParentFile().mkdirs();
		FileOutputStream fos = new FileOutputStream(output);
		fos.write(toWrite);
		fos.flush();
		fos.close();
		System.out.println("The file was successfully encrypted and stored in: " + output.getPath());
	}
	
	public byte[] getFileInBytes(File f) throws IOException{
		FileInputStream fis = new FileInputStream(f);
		byte[] fbytes = new byte[(int) f.length()];
		fis.read(fbytes);
		fis.close();
		return fbytes;
	}
}
