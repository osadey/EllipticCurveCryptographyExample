package com.oodrive.elliptic;

import static javax.xml.bind.DatatypeConverter.parseHexBinary;
import static javax.xml.bind.DatatypeConverter.printHexBinary;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.crypto.KeyAgreement;
import javax.crypto.spec.SecretKeySpec;



/**
 * Next generation fast File/Message End-to-end Encryption
 * Can be used in real-time.
 * Files Encryption/Decryption with: 
 * - Elliptic Curve Ephemeral Key Pair Agreement (EC/ECDH) - Instead of RSA Key pairs - Equivalent to RSA-7680 bits 
 * - AES-GCM File encryption with random Nonce, provided by a centralized service in order to add a second factor and
 *  avoid man-in-a-middle exchange of public keys (Instead of CBC)
 * 
 * Note: The JDK has also to be upgraded in order to manage such an encryption (JCE Policy 8 - Unlimited Strength Jurisdiction)
 *  
 * @author o.sadey
 *
 */
public class EllipticCurveKeyAgreement {
	
    // AES-GCM parameters
    public static final int GCM_NONCE_LENGTH = 12; // in bytes
	
	public static void main(String[] args) throws Exception {
	    
	    // Ask for the Encrypt)/Decrypt Mode
	    // Console input
	    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	    System.out.print("Please enter the mode. 'E' for Encrypt / 'D' for Decrypt: ");
	    String encryptDecryptMode = null;
	    try {
	    	encryptDecryptMode = reader.readLine();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		
	    // Generate ephemeral ECDH keypair
	    KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
	    kpg.initialize(384); // Equivalent to RSA 7680 bits 
	    KeyPair kp = kpg.generateKeyPair();
	    byte[] ourPk = kp.getPublic().getEncoded();

	    // Display our public key
	    System.out.printf("Public Key: %s%n", printHexBinary(ourPk));
	  	    
	    // Read other's public key
	    // Console input
	    reader = new BufferedReader(new InputStreamReader(System.in));
	    System.out.print("Please enter the other Public Key: ");
	    String consoleOtherPublicKey = null;
	    try {
	    	consoleOtherPublicKey = reader.readLine();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    byte[] otherPk = parseHexBinary(consoleOtherPublicKey);

	    KeyFactory kf = KeyFactory.getInstance("EC");
	    X509EncodedKeySpec pkSpec = new X509EncodedKeySpec(otherPk);
	    PublicKey otherPublicKey = kf.generatePublic(pkSpec);

	    // Perform key agreement
	    KeyAgreement ka = KeyAgreement.getInstance("ECDH");
	    ka.init(kp.getPrivate());
	    ka.doPhase(otherPublicKey, true);

	    // Read shared secret
	    byte[] sharedSecret = ka.generateSecret();
	    System.out.printf("Shared secret: %s%n", printHexBinary(sharedSecret));

	    // Derive a key from the shared secret and both public keys
	    MessageDigest hash = MessageDigest.getInstance("SHA-256");
	    hash.update(sharedSecret);
	    // Simple deterministic ordering
	    List<ByteBuffer> keys = Arrays.asList(ByteBuffer.wrap(ourPk), ByteBuffer.wrap(otherPk));
	    Collections.sort(keys);
	    hash.update(keys.get(0));
	    hash.update(keys.get(1));

	    byte[] derivedKey = hash.digest();
	    System.out.printf("Final secret key: %s%n", printHexBinary(derivedKey));
	    

	    SecretKeySpec derivedSecretKeySpec= new SecretKeySpec(derivedKey, "AES");
	    
	    // Encrypt Mode
	    if (encryptDecryptMode.equals("E")) {
	    	
	    	// Prepare the Nonce
			SecureRandom random = SecureRandom.getInstanceStrong();
	        final byte[] nonce = new byte[GCM_NONCE_LENGTH];
	        random.nextBytes(nonce);
		    
		    System.out.printf("Nonce sent to server: %s%n", printHexBinary(nonce));
	        
		    // Encrypt the file with the final/derived key
			File originalFile = new File("confidential.pdf");
			File encryptedFile = new File("EncryptedDocs/encryptedDoc");
			new EncryptData(originalFile, encryptedFile, derivedSecretKeySpec, nonce);
			
	    }
	    // Decrypt Mode
	    else if (encryptDecryptMode.equals("D")) {
	    	
	    	// Console input for the Nonce
		    reader = new BufferedReader(new InputStreamReader(System.in));
		    System.out.print("Please enter the Nonce (provided by the server): ");
		    String consoleOtherNonce = null;
		    try {
		    	consoleOtherNonce = reader.readLine();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		    
		    final byte[] otherNonce = parseHexBinary(consoleOtherNonce);
			
			// Decrypt the file with the final/derived key and the Nonce
			File encryptedFileReceived = new File("EncryptedDocs/encryptedDoc");
			File decryptedFile = new File("DecryptedDocs/decryptedDoc.pdf");
			new DecryptData(encryptedFileReceived, decryptedFile, derivedSecretKeySpec, otherNonce);
	    }
		
	    
	    
	  }
}
