Next generation fast File/Message End-to-end Encryption
Can be used in real-time.
Files Encryption/Decryption with: 
- Elliptic Curve Ephemeral Key Pair Agreement (EC/ECDH) - Instead of RSA Key pairs - Equivalent to RSA-7680 bits 
- AES-GCM File encryption with random Nonce, provided by a centralized service in order to add a second factor and
avoid man-in-a-middle exchange of public keys (Instead of CBC)

Note: The JDK has also to be upgraded in order to manage such an encryption (JCE Policy 8 - Unlimited Strength Jurisdiction)
 
Author: Offer SADEY